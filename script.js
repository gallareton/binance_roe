// ==UserScript==
// @name         Wyliczanie ROE na binance
// @namespace    http://tampermonkey.net/
// @version      1.0
// @description  Wyliczanie ROE podczas składania zamówienia limit lub market na binance
// @author       Gall
// @match        https://www.binance.com/*/futures/*
// @grant        none
// ==/UserScript==

(function() {
    setTimeout(function() {
    var leverage = parseInt(document.querySelectorAll('[data-testid="20x"]')[0].textContent.match(/\d+/)[0]);
    var currentPrice = parseFloat(document.getElementsByClassName('showPrice')[0].textContent.replace(/,/, ''));
    var tpElement = document.getElementById('FormRow-normal-takeProfit');
    var slElement = document.getElementById('FormRow-normal-stopLoss');
    var quantityElement = document.getElementById('FormRow-normal-quantity');
    function checkLeverage()
    {
        leverage = parseInt(document.querySelectorAll('[data-testid="20x"]')[0].textContent.match(/\d+/)[0]);
    };
    function checkPrice()
    {
        currentPrice = parseFloat(document.getElementsByClassName('showPrice')[0].textContent.replace(/,/, ''));
            calculateTp();
        calculateSl();
    };
    function isLimit()
    {
        if (document.querySelectorAll('.css-4463ze[data-testid="limit"]').length > 0) {
            return true;
        }
        return false;
    }
        function calculateTp()
        {
            var priceElement = document.getElementById('FormRow-normal-price');
            var price = 0;
            var quantity = 0;
            if (priceElement !== null) {
                if (priceElement.value !== '') {
                    price = parseFloat(priceElement.value);
                }
            }
            if (quantityElement !== null && quantityElement.value !== '') {
                quantity = parseFloat(quantityElement.value);
            }
            if (!isLimit()) {
                price = currentPrice;
            }
            var margin = quantity * price / leverage;
            if (tpElement === null || tpElement.value === '') {
                return;
            }
            var tp = parseFloat(tpElement.value);
            var tpRoe = Math.round((tp - price) / price * leverage * 100);
            if (tp < price) {
                tpRoe *= -1;
            }
            var tpValue = (tpRoe * margin / 100).toFixed(2);
            var color = null;
            if (tpRoe === 0) {
                color = 'white'
            } else if (tpRoe > 0) {
                color = 'green';
            } else {
                color = 'red';
            }
            var tpInfo = document.getElementById('myTpInfo');
            if (tpInfo === null) {
                tpInfo = document.createElement('div');
                tpInfo.id = 'myTpInfo';
                tpInfo.className = 'css-9m4cnv';
            }
            tpInfo.style.color = color;
            tpInfo.style.textAlign = 'center';
            tpInfo.textContent = tpValue + '$ (' + tpRoe + '%)';
            tpElement.closest('.css-9m4cnv').after(tpInfo);
        }
        function calculateSl()
        {
            var priceElement = document.getElementById('FormRow-normal-price');
            var price = 0;
        var quantity = 0;
        if (priceElement !== null) {
            if (priceElement.value !== '') {
                price = parseFloat(priceElement.value);
            }
        }
        if (quantityElement !== null && quantityElement.value !== '') {
            quantity = parseFloat(quantityElement.value);
        }
        if (!isLimit()) {
            price = currentPrice;
        }
        var margin = quantity * price / leverage;
            if (slElement === null || slElement.value === '') {
                return;
            }
        var sl = parseFloat(slElement.value);
        var slRoe = Math.round((sl - price) / price * leverage * 100);
            if (sl > price) {
                slRoe *= -1;
            }
        var slValue = (slRoe * margin / 100).toFixed(2);
        var color = null;
            if (slRoe === 0) {
                color = 'white'
            } else if (slRoe > 0) {
                color = 'green';
            } else {
                color = 'red';
            }
        var slInfo = document.getElementById('mySlInfo');
        if (slInfo === null) {
            slInfo = document.createElement('div');
            slInfo.id = 'mySlInfo';
            slInfo.className = 'css-9m4cnv';
        }
        slInfo.style.color = color;
        slInfo.style.textAlign = 'center';
        slInfo.textContent = slValue + '$ (' + slRoe + '%)';
        slElement.closest('.css-9m4cnv').after(slInfo);
        }

    var leverageInterval = setInterval(checkLeverage, 1000);
    var priceInterval = setInterval(checkPrice, 200);
    }, 5000);
})();